plugins {
    kotlin("multiplatform") version BuildSrcGlobal.VersionKotlin
    id("app.cash.sqldelight") version "app.cash.sqldelight:gradle-plugin".depVersionOnly()
    application
}

group = "com.hoffi"
version = "1.0.0"

val rootPackage: String by extra { "${rootProject.group}.${project.name.replace("[-_]".toRegex(), "").lowercase()}" }
val projectPackage: String by extra { rootPackage }
val theMainClass: String by extra { "Main" }
application {
    mainClass.set("${projectPackage}.${theMainClass}" + "Kt") // + "Kt" if fun main is outside a class
}

repositories {
    google()
    mavenCentral()
    maven(url = "https://oss.sonatype.org/content/repositories/snapshots")
}

sqldelight {
    databases {
        create("Database") {
            //version = 1 // meaning current is 2 (so run all migrations less than 2)
            dialect("app.cash.sqldelight:sqlite-3-38-dialect:" + "app.cash.sqldelight:gradle-plugin".depVersionOnly())
            packageName.set("com.hoffi.sqldelightscratch.db")
            sourceFolders.set(listOf("sqldelight"))
            schemaOutputDirectory.set(file("src/commonMain/sqldelight/migrations"))
            //deriveSchemaFromMigrations.set(true) // false = do NOT run migrations to generate <Name>.db
            //verifyMigrations.set(false) // true = fail if migrations won't succeed
        }
    }
}

kotlin {
    val hostOs = System.getProperty("os.name")
    val isMingwX64 = hostOs.startsWith("Windows")
    val nativeTarget = when {
        hostOs == "Mac OS X" -> macosX64("native")
        hostOs == "Linux" -> linuxX64("native")
        isMingwX64 -> mingwX64("native")
        else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
    }

    nativeTarget.apply {
        binaries {
            executable {
                entryPoint = "${projectPackage}.main"
            }
        }
    }
    jvm {
        jvmToolchain(BuildSrcGlobal.jdkVersion)
        withJava()
        testRuns["test"].executionTask.configure {
            useJUnitPlatform()
        }
    }
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("co.touchlab:kermit".depAndVersion())
                implementation("com.squareup.okio:okio".depAndVersion())
            }
        }
        val commonTest by getting {
            dependencies {
                //implementation(kotlin("test"))
                implementation("io.kotest:kotest-framework-engine".depButVersionOf("io.kotest:kotest-runner-junit5"))
                implementation("io.kotest:kotest-framework-datatest".depButVersionOf("io.kotest:kotest-runner-junit5"))
                implementation("io.kotest:kotest-assertions-core".depButVersionOf("io.kotest:kotest-runner-junit5"))
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        val jvmMain by getting {
            dependencies {
                implementation("app.cash.sqldelight:sqlite-driver".depAndVersion())
            }
        }
        val jvmTest by getting
        val nativeMain by getting {
            dependencies {
                implementation("app.cash.sqldelight:native-driver".depAndVersion())
            }
        }
        val nativeTest by getting
    }
}

/** create package dirs under each subprojects src/module/kotlin
 * based on subproject's extra property: projectPackage
 *
 * in rootProject:
 * //=============
 * group = "com.hoffi"
 * version = "1.0-SNAPSHOT"
 * val artifactName by extra { "${rootProject.name.toLowerCase()}-${project.name.toLowerCase()}" }
 * val rootPackage by extra { "${rootProject.group}.${rootProject.name.replace("[-_]".toRegex(), "").toLowerCase()}" }
 * val theMainClass by extra { "Main" }
 * application {
 *     mainClass.set("${rootPackage}.${theMainClass}" + "Kt") // + "Kt" if fun main is outside a class
 * }
 *
 * in subprojects:
 * //=============
 * group = "${rootProject.group}"
 * version = "${rootProject.version}"
 * val artifactName by extra { "${rootProject.name.toLowerCase()}-${project.name.toLowerCase()}" }
 * val rootPackage: String by rootProject.extra
 * val projectPackage by extra { "${rootPackage}.${project.name.toLowerCase()}" }
 * val theMainClass by extra { "Main" }
 * application {
 *     mainClass.set("${projectPackage}.${theMainClass}" + "Kt") // + "Kt" if fun main is outside a class
 * }
 */
val createSrcBasePackages = tasks.register("createSrcBasePackages") {
    doLast {
        project.allprojects.forEach { prj ->
            var relProjectDirString = prj.projectDir.toString().removePrefix(rootProject.projectDir.toString())
            if (relProjectDirString.isBlank()) { relProjectDirString = "ROOT" } else { relProjectDirString = relProjectDirString.removePrefix("/") }
            println("  in project: $relProjectDirString ...")
            val projectPackage: String by prj.extra
            val projectPackageDirString = projectPackage.split('.').joinToString("/")
            prj.pluginManager.let() { when {
                it.hasPlugin("org.jetbrains.kotlin.jvm") -> {
                    prj.sourceSets.forEach { sourceSet ->
                        val ssDir = File("${prj.projectDir}/src/${sourceSet.name}/kotlin")
                        if (ssDir.exists()) {
                            mkdir("$ssDir/$projectPackageDirString")
                        }
                    }
                }
                it.hasPlugin("org.jetbrains.kotlin.multiplatform") -> {
                    val kotlinMultiplatformExtension = prj.extensions.findByType(org.jetbrains.kotlin.gradle.dsl.KotlinMultiplatformExtension::class.java)
                    val kotlinProjectExtension = kotlinMultiplatformExtension as org.jetbrains.kotlin.gradle.dsl.KotlinProjectExtension
                    //prj.kotlin.sourceSets.forEach {
                    kotlinProjectExtension.sourceSets.forEach { topKotlinSourceSet ->
                        kotlin.sourceSets.forEach { kotlinSourceSet ->
                            val ssDir = File("${prj.projectDir}/src/${topKotlinSourceSet.name}/kotlin")
                            if (ssDir.exists()) {
                                mkdir("$ssDir/$projectPackageDirString")
                            }
                        }
                    }
                }
            } }
            println("  in project: $relProjectDirString ok.")
        }
    }
}
