package com.hoffi.sqldelightscratch

import com.hoffi.sqldelightscratch.mpp.dbPath
import com.hoffi.sqldelightscratch.mpp.fs
import okio.Path

fun Path.nonEmpty(): Boolean {
    val dbFileMetadata = fs.metadataOrNull(dbPath)
    return ! ((dbFileMetadata == null) || (dbFileMetadata.size == null) || !dbFileMetadata.isRegularFile || (dbFileMetadata.size!! <= 0))
}
fun Path.notExistingOrEmpty() = ! this.nonEmpty()
