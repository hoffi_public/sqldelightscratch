package com.hoffi.sqldelightscratch

import com.hoffi.sqldelightscratch.db.*
import com.hoffi.sqldelightscratch.mpp.MppPlatform

fun mainCommon(args: Array<String>) {
    println("Hello, $MppPlatform!")
    println("Program arguments: ${args.joinToString()}")

    val database = getOrCreateMigratedDatabase()
    //DbDataMigrations.migrate()

    val hockeyQueries: HockeyQueries = database.hockeyQueries
    println("before: ")
    hockeyQueries.selectAll().executeAsList().forEachIndexed { i, hockeyPlayer ->
        println(i.toString().padStart(4) + ":" + hockeyPlayer.toString())
    }
// Prints [HockeyPlayer(5, "Ryan Getzlaf")]
    hockeyQueries.deleteAbove(9)
    println("after :")
    hockeyQueries.selectAll().executeAsList().forEachIndexed { i, hockeyPlayer ->
        println(i.toString().padStart(4) + ":" + hockeyPlayer.toString())
    }
// Prints [HockeyPlayer(5, "Ryan Getzlaf")]

    hockeyQueries.insert(player_number = 10, full_name = "Corey Perry")
    //val player = HockeyPlayer(11, "Ronald McDonald") // TODO v1
    val player = HockeyPlayer(11, "Perry McDonald", -1, -1) // TODO v2
    hockeyQueries.insertFullPlayerObject(player)

    println("finally: ")
    hockeyQueries.selectAll().executeAsList().forEachIndexed { i, hockeyPlayer ->
        println(i.toString().padStart(4) + ":" + hockeyPlayer.toString())
    }
    // TODO v2
    println("drafted 1983: ")
    hockeyQueries.selectByDraftYear(1983).executeAsList().forEachIndexed { i, hockeyPlayer ->
        println(i.toString().padStart(4) + ":" + hockeyPlayer.toString())
    }
    println("starting with: capital G")
    hockeyQueries.selectMatching("% G%").executeAsList().forEachIndexed { i, hockeyPlayer ->
        println(i.toString().padStart(4) + ":" + hockeyPlayer.toString())
    }
    println("firstOrLastname with: 'Perry'")
    hockeyQueries.firstOrLastName("Perry").executeAsList().forEachIndexed { i, hockeyPlayer ->
        println(i.toString().padStart(4) + ":" + hockeyPlayer.toString())
    }
}
