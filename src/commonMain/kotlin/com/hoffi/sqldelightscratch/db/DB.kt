package com.hoffi.sqldelightscratch.db

import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.logs.LogSqliteDriver
import co.touchlab.kermit.Logger
import com.hoffi.sqldelightscratch.mpp.dbPath
import com.hoffi.sqldelightscratch.notExistingOrEmpty

expect class DriverFactory() {
    fun createDriver(): SqlDriver
}
object DBDriver {
    fun driver(): SqlDriver = DriverFactory().createDriver()
    fun loggingDriver(): SqlDriver = LogSqliteDriver(sqlDriver = DriverFactory().createDriver()) { logMessage ->
        Logger.i { logMessage }
    }
}

/** Database.Schema.version always is ONE GREATER THAN the max <x>.sqm migration that was run</br>
 * so Database.Schema.version = 2 ==> max migration that will be (was) run: 1.sqm </br>
 * also: if Database.Schema.version = 2 nothing greater OR EQUAL(!!!) than 1.sqm will run! */
fun getOrCreateMigratedDatabase(): Database {
    val driver = DBDriver.driver()
    //val driver = DBDriver.loggingDriver()

    val database = Database(driver)
    if (dbPath.notExistingOrEmpty()) {
        Logger.i("creating $dbPath")
        Database.Schema.create(driver)
    }
    // run migrations
    Logger.i("checking if migrations have to be run")
    DbDataMigrations.migrate(driver, database)
    println("Database: $dbPath (v${Database.Schema.version})")

    return database
}

