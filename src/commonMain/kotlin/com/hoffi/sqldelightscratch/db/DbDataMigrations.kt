package com.hoffi.sqldelightscratch.db

import app.cash.sqldelight.db.AfterVersion
import app.cash.sqldelight.db.QueryResult
import app.cash.sqldelight.db.SqlDriver
import co.touchlab.kermit.Logger
import com.hoffi.sqldelightscratch.mpp.dbPath

/** Database.Schema.version always is ONE GREATER THAN the max <x>.sqm migration that was run</br>
 * so Database.Schema.version = 2 ==> max migration that will be (was) run: 1.sqm </br>
 * also: if Database.Schema.version = 2 nothing greater OR EQUAL(!!!) than 1.sqm will run! */
object DbDataMigrations {
    operator fun AfterVersion.plus(other: AfterVersion): AfterVersion {
        if (this.afterVersion != other.afterVersion) throw Exception("different AfterVersions")
        return AfterVersion(this.afterVersion) { sqlDriver ->
            sqlDriver.apply(this.block).also { sqlDriver.apply(other.block) }
        }
    }

    fun migrate(driver: SqlDriver, database: Database) {
        val _0latestMigration = database.hockeyQueries._0latestMigration().executeAsOne().max?.toInt() ?: throw Exception("no table _0latestMigration found or no entries in it")
        val currentPersistentSchemaVersion = _0latestMigration+1
        if (currentPersistentSchemaVersion >= Database.Schema.version) return
        Logger.i("migrating $dbPath from v$currentPersistentSchemaVersion -> v${Database.Schema.version} (current)")
        // ============================================================================================================
        // =====   code migrations below are run AFTER the corresponding fromVersion.sqm was executed             =====
        // ============================================================================================================
        Database.Schema.migrate(driver, oldVersion = _0latestMigration+1, newVersion = Database.Schema.version,
            migrateInternalDdl(driver, toVersion = 1,
                "ALTER TABLE hockeyPlayer ADD COLUMN draft_year INTEGER DEFAULT -1",
                "ALTER TABLE hockeyPlayer ADD COLUMN draft_order INTEGER DEFAULT -1",
            ) +
            migrateInternal(driver, toVersion = 1,
                "UPDATE hockeyPlayer set draft_order = -2 where draft_order = -1",
                "INSERT INTO hockeyPlayer (player_number, full_name, draft_year, draft_order) VALUES (5, 'Ryan Getzlaf', 2003, 19) ON CONFLICT(player_number) DO UPDATE set draft_year = 2003, draft_order = 19 where player_number = 5 and full_name = 'Ryan Getzlaf'",
                "INSERT INTO hockeyPlayer (player_number, full_name, draft_year, draft_order) VALUES (9, 'Wayne Gretzky', 1979, -1) ON CONFLICT(player_number) DO NOTHING;",
                "INSERT INTO hockeyPlayer (player_number, full_name, draft_year, draft_order) VALUES (8, 'Uwe Krupp', 1983, 214) ON CONFLICT(player_number) DO NOTHING",
            ),
            migrateInternal(driver, toVersion = 2, {
                throw Exception("proof that this translation does NOT run!")
                //driver.execute(null, "INSERT INTO ...", 0)
            })
        )
    }

    private fun migrateInternalDdl(driver: SqlDriver, toVersion: Int, vararg driverExecBlocks: String): AfterVersion {
        return AfterVersion(toVersion) {
            val re = "^\\s*ALTER TABLE ([^ ]+) ([^ ]+) ([^ ]+) ([^ ]+).*$".toRegex(RegexOption.IGNORE_CASE)
            for (driverExecBlock in driverExecBlocks) {
                val matchResult = re.matchEntire(driverExecBlock)
                if (matchResult == null) {
                    driver.execute(null, driverExecBlock, 0)
                } else {
                    val tableName = matchResult.groupValues[1]
                    val op = matchResult.groupValues[2].uppercase()
                    val on = matchResult.groupValues[3].uppercase()
                    val colName = matchResult.groupValues[4]
                    when (op) {
                        "ADD" -> {
                            when (on) {
                                "COLUMN" -> try {
                                    val queryResult = driver.execute(null, "SELECT $colName FROM $tableName LIMIT 1", 0)
                                } catch (e: Exception) {
                                    // no such column
                                    driver.execute(null, driverExecBlock, 0)
                                }
                            }
                        }
                        else -> driver.execute(null, driverExecBlock, 0)
                    }
                }
            }
        }
    }

    private fun migrateInternal(driver: SqlDriver, toVersion: Int, vararg driverExecBlocks: SqlDriver.() -> QueryResult<Long>): AfterVersion {
        return AfterVersion(toVersion) {
            Logger.i("running AfterVersion ${DbDataMigrations::class.simpleName}.migrate($toVersion)")
            for(driverExecBlock in driverExecBlocks) {
                driver.apply { driverExecBlock() }
            }
            driver.execute(null, "UPDATE _0latestMigration set latestMigration = $toVersion WHERE id = 1", 0)
        }
    }

    private fun migrateInternal(driver: SqlDriver, toVersion: Int, vararg driverExecBlocks: String): AfterVersion {
        return AfterVersion(toVersion) {
            Logger.i("running AfterVersion ${DbDataMigrations::class.simpleName}.migrate($toVersion)")
            for(driverExecBlock in driverExecBlocks) {
                driver.execute(null, driverExecBlock, 0)
            }
            driver.execute(null, "UPDATE _0latestMigration set latestMigration = $toVersion WHERE id = 1", 0)
        }
    }
}
