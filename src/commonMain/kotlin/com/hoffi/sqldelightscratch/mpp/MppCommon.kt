package com.hoffi.sqldelightscratch.mpp

import okio.FileSystem
import okio.Path

expect val dbPath: Path
expect val MppPlatform: String
expect val fs: FileSystem
expect fun cwd(): Path

