package com.hoffi.sqldelightscratch.db

import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.driver.jdbc.sqlite.JdbcSqliteDriver
import app.cash.sqldelight.logs.LogSqliteDriver
import com.hoffi.sqldelightscratch.mpp.dbPath

actual class DriverFactory {
    actual fun createDriver(): SqlDriver {
//        val driver: SqlDriver = JdbcSqliteDriver(JdbcSqliteDriver.IN_MEMORY)
        val driver: SqlDriver = JdbcSqliteDriver("jdbc:sqlite:${dbPath}")
        return driver
    }
}
