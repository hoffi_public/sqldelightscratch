package com.hoffi.sqldelightscratch.mpp

import okio.FileSystem
import okio.Path
import okio.Path.Companion.toPath
import java.io.File

actual val dbPath = "sqlite.db".toPath()
actual val MppPlatform = "JVM"
actual val fs: FileSystem = FileSystem.SYSTEM
actual fun cwd(): Path = File(".").absolutePath.toPath(normalize = true) // (File(File(".").absolutePath).canonicalPath.toPath()
