package com.hoffi.sqldelightscratch.db

import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.driver.native.NativeSqliteDriver
import co.touchlab.kermit.Logger
import com.hoffi.sqldelightscratch.mpp.dbPath

actual class DriverFactory {
    actual fun createDriver(): SqlDriver {
        return NativeSqliteDriver(Database.Schema, dbPath.toString())
    }
}
